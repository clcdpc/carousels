﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carousels.Models
{
    public class CarouselItem
    {
        public string CatalogLink { get; set; }
        public string ImageUrl { get; set; }
    }
}